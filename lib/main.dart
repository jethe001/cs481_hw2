import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Warren Buffett Portfolio',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      home: MyHomePage(title: 'Warren Buffett Portfolio'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  const MyHomePage({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Column(

        //mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,

        children: <Widget>[
          CircleAvatar(
            backgroundImage: AssetImage(
              'images/wb.jpg',

            ),
            radius: 140.0,
          ),
          Text(
            "Warren Buffett",
            style: TextStyle(
              color: Colors.blueGrey,
              fontWeight: FontWeight.bold,
              fontSize: 40.0,
            ),
          ),
          Text(
            "Business Tycoon and investor",
            style: TextStyle(
              color: Colors.green,
              fontSize: 20.0,
              letterSpacing: 2.5,
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 50.0),
            child: Divider(
              color: Colors.yellow,
            ),
          ),
          InfoBox(
            text: "warren@buffett.com",
            icon: Icons.email,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30.0, right: 30.0),
            child: Text("\nWarren Edward Buffett is an American investor, business tycoon, philanthropist, and the chairman and CEO of Berkshire Hathaway. "
                "He is considered one of the most successful investors in the world and has a net worth of US dollars 78.9 billion as of August 2020.",
              style: new TextStyle(
                color: Colors.green,
                fontSize: 16.0,
              ),
            ),
          )

        ],
      ),
    );
  }
}

class InfoBox extends StatelessWidget {
  final String text;
  final IconData icon;
  const InfoBox({
    Key key,
    this.text,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 10.0,
        horizontal: 25.0,
      ),
      height: 50.0,
      child: Material(
        elevation: 10.0,
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Icon(
                icon,
                color: Colors.red,


              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0),
              child: Text(
                text,
                style: TextStyle(
                  color: Colors.green,
                  fontSize: 16.0,
                ),
              ),
            )

          ],

        ),

      ),
    );
  }
}
